import torch

from models.discriminator import DiscriminatorNet

class GeneratorNet(torch.nn.Module):
    
    def __init__(self,
                 latent_dims=10,
                 output_dims=28,
                 layers=3,
                 width=[256,512,1024],
                 batch_size=100):
        """
        noise - uniform distribution
        -> mlp
        -> output
        """
        super(GeneratorNet,self).__init__()

        self.latent_dims = latent_dims
        self.output_dims = output_dims**2
        self.batch_size = batch_size

        net = []
        prev_dims = self.latent_dims
        for i in range(layers):
            net.append(torch.nn.Linear(prev_dims, width[i]))
            prev_dims = width[i]
            if i % 2 == 0:
                net.append(torch.nn.ReLU())
            else:
                net.append(torch.nn.ReLU())
            net.append(torch.nn.Dropout(0.2))
            
        net.append(torch.nn.Linear(width[-1],self.output_dims))
        net.append(torch.nn.ReLU())
        
        self.network = torch.nn.Sequential(*net)
        
        self.loss = torch.nn.BCELoss()
        
        return
    
    
    def forward(self, latents):
        """
        latents = torch.rand((self.batch_size, self.latent_dims))
        """
        return self.network(latents)
    
    def generate_z(self):
        latents = torch.randn((self.batch_size, self.latent_dims)).cuda()
        return self.forward((latents))
       
       
    def compute_loss(self, gen_z, discriminator:DiscriminatorNet):
        l = self.loss(discriminator.forward(gen_z).squeeze(), torch.ones(len(gen_z)).cuda())
        return l
        