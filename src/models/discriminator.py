import torch


class DiscriminatorNet(torch.nn.Module):
    
    
    def __init__(self,
                 input_dims=28,
                 output_dims=28,
                 layers=3,
                 width=[1024,512,256]):
        """
        generated samples, true samples
        mlp
        classifiers 1:true 0:generated
        """
        super(DiscriminatorNet,self).__init__()
        
        self.input_dims = input_dims**2
        
        net = []
        prev_dims = self.input_dims
        for i in range(layers):
            net.append(torch.nn.Linear(prev_dims, width[i]))
            prev_dims = width[i]
            net.append(torch.nn.ReLU())
            
        net.append(torch.nn.Linear(width[-1],1))
        net.append(torch.nn.Sigmoid())
        
        self.network = torch.nn.Sequential(*net)
        
        self.loss = torch.nn.BCELoss()
        
        return
    
    
    def forward(self, inputs):
        return self.network(inputs)
    
    
    def compute_loss(self, preds, truth):
        return self.loss(preds,truth)