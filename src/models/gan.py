import torch
import torchvision
from tqdm import tqdm
from collections import OrderedDict
import os
import numpy as np

from models.discriminator import DiscriminatorNet
from models.generator import GeneratorNet




class Gan(torch.nn.Module):
    
    def __init__(self,
                 config,
                 epochs=1001,
                 k_iters=1,
                 batch_size=100,
                 lr=0.0005
                 ):
        
        super(Gan,self).__init__()
        
        self.epochs = epochs
        self.k_iters = k_iters
        self.batch_size = batch_size
        self.lr=lr
        self.discriminator:DiscriminatorNet
        self.generator:GeneratorNet
        
        if config['discriminator']:
            self.discriminator = DiscriminatorNet(**config['discriminator']).cuda()
        else:
            self.discriminator = DiscriminatorNet().cuda()
            
        if config['generator']:
            self.generator = GeneratorNet(**config['generator']).cuda()
        else:
            self.generator = GeneratorNet().cuda()
            
        self.gen_opt = torch.optim.Adam(params=self.generator.parameters(), lr=self.lr)
        self.dis_opt = torch.optim.Adam(params=self.discriminator.parameters(), lr=self.lr)
        
        
    def train_descriminator(self, samples, gen_z):
        self.dis_opt.zero_grad()
        
        samples = torch.flatten(samples, start_dim=1)
        
        sample_labels = torch.ones(len(samples))
        gen_z_labels = torch.zeros(len(samples))
        
        inputs = torch.concat((samples,gen_z))
        labels = torch.concat((sample_labels, gen_z_labels)).cuda()
        
        preds = self.discriminator(inputs)
        loss = self.discriminator.loss(preds.squeeze(), labels)
        
        # loss.backward(retain_graph=True)
        loss.backward()
        self.dis_opt.step()
    
        return loss.cpu().item()
    
    def train_generator(self, gen_z):
        self.gen_opt.zero_grad()
        
     
        loss = self.generator.compute_loss(gen_z, self.discriminator)
        
        loss.backward()
        self.gen_opt.step()
    
        return loss.cpu().item()
        
    def train_model(self):
        
 
        for epoch in range(self.epochs):
            
            
            train_loader = torch.utils.data.DataLoader(
                        torchvision.datasets.MNIST(f'{os.path.dirname(__file__)}/data', train=True, download=True,
                                                    transform=torchvision.transforms.Compose([
                                                    torchvision.transforms.ToTensor(),
                                                    ])),
                        batch_size=self.batch_size, shuffle=True)
            
            
            stats = OrderedDict()
            stats['gen_loss'] = 0
            stats['dis_loss'] = 0
            
            progress_bar = tqdm(train_loader, desc='| Epoch {:03d}'.format(epoch), leave=False, disable=False)


            for i, (image,_) in enumerate(progress_bar):
                
                
                gen_z = self.generator.generate_z()
                
                stats['dis_loss'] += self.train_descriminator(image.cuda(), gen_z)
                
                if i % 1==0:
                    gen_z = self.generator.generate_z()
                    stats['gen_loss'] += self.train_generator(gen_z)
                
                # print(stats['dis_loss'], stats['gen_loss'])
    
                progress_bar.set_postfix({key: '{:.4g}'.format(value / (i + 1)) for key, value in stats.items()},refresh=True)

            if epoch % 10 == 0:
                self.plot_generated_images(epoch)
        return
    
    
    
    def plot_generated_images(self, epoch):
        
        import matplotlib.pyplot as plt
        
        gen_z = self.generator.generate_z()
        
        gen_z = gen_z.view((100,1,28,28)).cpu().detach()

        img_grid = torchvision.utils.make_grid(gen_z)
        np_grid = img_grid.numpy()
        plt.imshow(np.transpose(np_grid, (1,2,0)), interpolation='nearest')
        plt.axis('off')
    
        # fig = plt.figure()
        # for i in range(100):
        #     plt.subplot(10,10,i+1)
        #     plt.tight_layout()
        #     plt.imshow(gen_z[i][0], cmap='gray', interpolation='none')
        #     plt.xticks([])
        #     plt.yticks([])
        
      
        plt.savefig(f'./outs_n_{epoch}.png')
       
        return