from models.gan import Gan



config = {
    "discriminator":None,
    "generator":None
}


gan = Gan(config)

gan.train_model()